//libraries
import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
//components
// import ProtectedHeader from './components/structural/headers/ProtectedHeader';
import PrivateRoute from './components/structural/PrivateRoute';
import Login from "./components/available/Login";
import Settings from "./components/available/Settings";
import Index from "./components/Index";
import SignUp from "./components/available/SignUp";
import PageNotFound from "./components/available/PageNotFound";
import Welcome from "./components/Welcome";
import ResetPassword from "./components/available/ResetPassword";
//hooks

export default function App() {

    return (
        <Router>
            <Switch>
                <Route exact path='/' component={Welcome}/>
                <Route path="/login" component={Login}/>
                <Route path="/reset-password" component={ResetPassword}/>
                <Route path="/sign-up" component={SignUp}/>
                <Route path="/settings" component={Settings}/>
                {/*<PrivateRoute path={'/404'} component={PageNotFound}/>*/}

                <PrivateRoute path={'/dashboard'} component={Index}/>
                <Route component={PageNotFound}/>
            </Switch>
        </Router>
    );
}
