import React from 'react';
import logo from "../../../../public/image/spotify-logo.png";

export default function Logo() {
    return (
        <div className={'logo-container'}>
            <img src={logo} alt="logo" title='logo'/>
            <div className="logo-text noselect">Music Room®</div>
        </div>
    );
}