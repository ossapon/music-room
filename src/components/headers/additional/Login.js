import React from 'react';
import {Link} from "react-router-dom";
import '../../../public/css/Headers.css';

export default function Login() {

    function goBack() {
        window.history.back();
    }

    return (
        <div className={'login-container'}>
            <Link to='/' onClick={goBack}>
                <div className={'login-item noselect home'}>back</div>
            </Link>
            <Link to="/sign-up">
                <div className={'login-item noselect'}>Sign Up</div>
            </Link>
            <Link to="/reset-password">
                <div className={'login-item noselect'}>Reset password</div>
            </Link>
            <Link to="/settings">
                <div className={'login-item noselect'}>Settings</div>
            </Link>
        </div>
    );
}