import React from 'react';

import Login from "./additional/Login";
import SignUp from "./additional/SignUp";
import Home from "./additional/Home";

export default function AvailableHeader({route}) {
    switch (route) {
        case 'login':
            return <Login/>;
        case 'sign-up':
            return <SignUp/>;
        case 'reset-password':
            return <Login/>;
        default:
            return <Home/>

    }
}
