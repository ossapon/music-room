import React from 'react';
import { Link } from "react-router-dom";

export default function ProtectedHeader () {
    return (
        <nav>
            <ul>
                <li>
                    <Link to={'/'}>Home</Link>
                </li>
                <li>
                    <Link to='/login'>Login</Link>
                </li>
                {/*<li>*/}
                {/*    <Link to='/sign-up'>Sign Up</Link>*/}
                {/*</li>*/}
                {/*<li>*/}
                {/*    <Link to={'/welcome'}>Welcome</Link>*/}
                {/*</li>*/}
                <li>
                    <Link to={'/settings'}>Settings</Link>
                </li>
            </ul>
        </nav>
    )
}