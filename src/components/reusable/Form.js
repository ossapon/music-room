import React from 'react';

export default function Form() {
    return (
        <form className={'form'}>
            <div className="facebook noselect">log in with facebook</div>
            <div className="or-text noselect">or</div>
            <div className="form-item">
                <input type="text" id={'user'} placeholder={'Email or username'}/>
            </div>
            <div className="form-item">
                <input type="password" id={'password'} placeholder={'Password'}/>
            </div>
            <div className="form-item rememberMe">
                <label htmlFor="rememberMe" className={'noselect'}>Remember me</label>
                <label className="switch">
                    <input type="checkbox" id="rememberMe"/>
                    <span className="slider round">
                    </span>
                </label>
            </div>
            <div className="form-item log-in-button noselect">LOG IN</div>
        </form>
    )
}