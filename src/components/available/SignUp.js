import React from 'react';
import Header from "../headers/Header";
import BackgroundPillars from "./styled/BackgroundPillars";
import Logo from "../headers/additional/logo/Logo";

// import AvailableHeader from "../headers/AvailableHeader";

export default function SignUp () {

    function signUp(e) {
        e.preventDefault();

        console.log(e.target);
        const form = e.target.parent;
        console.log(form);


    }

    return (
        <div className={'root-container'}>
            <BackgroundPillars/>
            <Logo/>
            <div style={{
                marginTop: '25px',
                color: 'rgb(171, 171, 171)',
                textAlign: 'center'
            }}>Fill this small form to enjoy the best music!</div>
            <form className={'form'}>
                <div className={'form-item'}>
                    <input required type="text" id={'username'} placeholder={'Username'}/>
                </div>
                <div className={'form-item'}>
                    <input required type="text" id={'email'} placeholder={'Email'}/>
                </div>
                <div className={'form-item'}>
                    <input required type="password" id={'password'} placeholder={'Password'}/>
                </div>
                <div className={'form-item'}>
                    <input required type="password" id={'re-password'} placeholder={'Repeat password'}/>
                </div>
                <div className="form-item log-in-button noselect" onClick={signUp}>LOG IN</div>
            </form>
            <Header route={'sign-up'}/>
            sign-up
        </div>
    )
}