import React from 'react';
// import AvailableHeader from "../headers/AvailableHeader";
import Logo from "../headers/additional/logo/Logo";
import Form from "../reusable/Form";
import Header from "../headers/Header";
import BackgroundPillars from "./styled/BackgroundPillars";

export default function Login () {
    return (
        <div className={'root-container'}>
            <BackgroundPillars/>
            <Logo/>
            <Form/>
            <Header route={'login'}/>
            {/*<AvailableHeader route={'login'}/>*/}
        </div>
    )
}