const users = require('./users');

module.exports = (router) => {
    console.log('routes.index: ', router);
    users(router);
    return router;
};