const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const User = require('../models/users');
const connUri = process.env.MONGO_LOCAL_CONN_URL;
const jwt = require('jsonwebtoken');

module.exports = {
    add: (req, res) => {
        mongoose.connect(connUri, { useNewUrlParser: true }, err => {
            let result = {};
            let status = 201;

            console.log({connUri, tst: { useNewUrlParser: true }, err});
            if (!err) {
                console.log("lol")
                const { username, password } = req.body;
                const user = new User({username, password});

                user.save((err, user) => {
                    if (!err) {
                        result.result = user;
                        result.status = status;
                    } else {
                        status = 500;
                        result.status = status;
                        result.error = err;
                    }
                    res.status(status).send(result);
                });
            } else {
                status = 500;
                result.status = status;
                result.error = err;
                res.status(status).send(result);
            }
        });

    },

    login: (req, res) => {
        const { username, password } = req.body;
        mongoose.connect(connUri, {useNewUrlParser: true}, err => {
            let result = {};
            let status = 200;

            if (!err) {
                User.findOne({username}, async (err, user) => {
                    if (!err && user) {
                        const match = await bcrypt.compare(password, user.password);
                        if (match) {
                            const payload = { user: user.username };
                            const options = { expiresIn: '2d', issuer: 'https://scotch.io' };
                            const secret = process.env.JWT_SECRET;

                            result.token = jwt.sign(payload, secret, options);
                            result.status = status;
                            result.result = user;
                        } else {
                            status = 401;
                            result.status = status;
                            result.error = 'Authentication error';
                        }
                        res.status(status).send(result);
                    } else {
                        status = 404;
                        result.status = status;
                        result. error = err;
                        res.status(status).send(result);
                    }
                })
            } else {
                status = 500;
                result.status = 500;
                result.error = err;
                res.status(status).send(result);
            }
        })
    },

    getAll: (req, res) => {
        mongoose.connect(connUri, {useNewUrlParser: true}, err => {
            User.find({}, (err, users) => {
                if (!err) {
                    res.send(users);
                } else {
                    console.log("Error: ", err);
                }
            })
        })
    }
};