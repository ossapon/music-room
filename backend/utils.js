const jwt = require('jsonwebtoken');

module.exports = {
    validateToken: (req, res, next) => {
        const authorizationHeader = req.headers.authorization;
        let result;

        if (authorizationHeader) {
            const token = req.headers.authentication.split(' ')[1];
            const options = {
                expiresIn: '2d',
                issuer: 'https://scotch.io'
            };

            try {
                result = jwt.verify(token, process.env.JWT_SECRET, options);
                req.decode = result;
                next();
            } catch (err) {
                throw new Error(err);
            }
        } else {
            result = {
                error: `Authentication error. Token required.`,
                status: 401
            };
            res.status(401).send(result);
        }
    }
};