import {useState}  from 'react';

export default function useForm (callback) {
    const [inputs, setInputs] = useState({});

    function handleSubmit(e) {
        if (e) {
            e.preventDefault();
        }
        callback();
    }

    function handleInputChange(e) {
        e.persist();
        const value = (e.target.getAttribute('type') === 'checkbox') ? e.target.checked : e.target.value.trim();
        setInputs(inputs => ({ ...inputs, [e.target.name]: value }))
    }

    return {
        handleSubmit,
        handleInputChange,
        inputs
    };
}
