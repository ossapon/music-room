import { useState, useEffect } from 'react';

export default function useGetFetch(url, headers = null) {
    const [data, setData] = useState([]);

    // async function getData() {
    //     const response = await fetch(url);
    //     const data = await response.json();
    //     setData(data);
    // }

    const user = new Promise(
        function (resolve, reject) {
            resolve(false);
        }
    );
    function getData () {
        user
            .then((result) => setData(result))
            .catch(error => setData(error))
    }
    useEffect(() => {
        getData();
    });
    return data;
}