import React from 'react';
import {
    Route,
    Redirect,
} from 'react-router-dom'
import useGetFetch from '../../hooks/useGetFetch';

// import Index from './components/structural/Index';
// import SignUp from './components/structural/SignUp';
// import Settings from './components/structural/Settings';
// import Login from './components/structural/Login';
// import PageNotFound from './components/structural/PageNotFound';

export default function PrivateRoute({ component: Component, ...rest }) {
    const isLogin = useGetFetch("http://localhost:4000/api/v1/users");
    return (
        <Route {...rest} render={(props) => (
            isLogin
                ? <Component {...props}/>
                : <Redirect to={'/'}/>
        )}/>
    );
}