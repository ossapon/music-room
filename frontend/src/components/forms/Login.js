import React from 'react';
import useForm from "../../hooks/useForm";

export default function Login() {

    async function LoginFetchTemp() {
        const response = await fetch("http://localhost:4000/api/v1/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputs)
        });
        console.log(response);
        let data = await response.json();
        console.log(data);
    }

    const { inputs, handleInputChange, handleSubmit} = useForm(LoginFetchTemp);

    return (
        <form className={'form'} onSubmit={handleSubmit}>
            <div className="facebook noselect">log in with facebook</div>
            <div className="or-text noselect">or</div>
            <div className="form-item">
                <input
                    required
                    type="text"
                    id={'username'}
                    name={'username'}
                    placeholder={'Email or username'}
                    onChange={handleInputChange}
                    value={inputs.username || ''}
                />
            </div>
            <div className="form-item">
                <input
                    required
                    type="password"
                    name={'password'}
                    id={'password'}
                    placeholder={'Password'}
                    onChange={handleInputChange}
                    value={inputs.password || ''}
                />
            </div>
            <div className="form-item rememberMe">
                <label htmlFor="rememberMe">Remember me</label>
                <label className="switch noselect">
                    <input
                        onChange={handleInputChange}
                        value={inputs.remember || ''}
                        type="checkbox"
                        name="remember"
                        id="rememberMe"
                        className={'noselect'}/>
                    <span className="slider round noselect">
                    </span>
                </label>
            </div>
            <button type={'submit'} className="form-item log-in-button noselect">LOG IN</button>
        </form>
    )
}