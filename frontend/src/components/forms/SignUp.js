import React from 'react';
import useForm from '../../hooks/useForm';


export default function SignUp() {

    const { inputs, handleInputChange, handleSubmit} = useForm();

    return (
        <form className={'form'} onSubmit={handleSubmit}>
            <div className={'form-item'}>
                <input
                    required
                    type="text"
                    id={'username'}
                    name={'username'}
                    placeholder={'Username'}
                    onChange={handleInputChange}
                    value={inputs.username || ''}
                />
            </div>
            <div className={'form-item'}>
                <input
                    required
                    type="email"
                    id={'email'}
                    name={'email'}
                    placeholder={'Email'}
                    onChange={handleInputChange}
                    value={inputs.email || ''}
                />
            </div>
            <div className={'form-item'}>
                <input
                    required
                    type="password"
                    id={'password'}
                    name={'password'}
                    placeholder={'Password'}
                    onChange={handleInputChange}
                    value={inputs.password || ''}
                />
            </div>
            <div className={'form-item'}>
                <input
                    required
                    type="password"
                    id={'re-password'}
                    name={'rePassword'}
                    placeholder={'Repeat password'}
                    onChange={handleInputChange}
                    value={inputs.rePassword || ''}
                />
            </div>
            <button type={'submit'} className="form-item log-in-button noselect">SIGN UP</button>
        </form>
    )
}