import React from 'react';
import Header from "../headers/Header";
import BackgroundPillars from "./styled/BackgroundPillars";
import Logo from "../headers/additional/logo/Logo";
import Form from "../forms/SignUp";
// import AvailableHeader from "../headers/AvailableHeader";

export default function SignUp () {
    return (
        <div className={'root-container'}>
            <BackgroundPillars/>
            <Logo/>
            <div style={{
                marginTop: '25px',
                color: 'rgb(171, 171, 171)',
                textAlign: 'center'
            }}>Fill this small form to enjoy the best music!</div>
            <Form/>
            <Header route={'sign-up'}/>
        </div>
    )
}