import React from 'react';
// import logo from '../../public/image/spotify-logo.png';
import Logo from './headers/additional/logo/Logo';
import Header from "./headers/Header";
import BackgroundPillars from './available/styled/BackgroundPillars';

export default function Welcome() {
    return (
        <div className={'root-container'}>
            <BackgroundPillars/>
            <Logo/>
            <div className={'tagline-text noselect'}>Play any song, any time, free</div>
            <Header route={'home'}/>
        </div>
    )
}