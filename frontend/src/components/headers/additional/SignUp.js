import React from 'react';
import {Link} from "react-router-dom";
import '../../../public/css/Headers.css';

export default function SignUp() {
    return (
        <div className={'login-container'}>
            <Link to="/">
                <div className={'login-item noselect'}>Home</div>
            </Link>
            <Link to="/login">
                <div className={'login-item noselect'}>Log In</div>
            </Link>
            <Link to="/reset-password">
                <div className={'login-item noselect'}>Reset password</div>
            </Link>
            <Link to="/settings">
                <div className={'login-item noselect'}>Settings</div>
            </Link>
        </div>
    );
}