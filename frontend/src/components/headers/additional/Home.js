import React from 'react';
import {Link} from "react-router-dom";
import '../../../public/css/Headers.css';

export default function Home() {
    return (
        <div className={'welcome-container'}>
            <Link to="/sign-up">
                <div className={'welcome-sign-up welcome-item noselect'}>Sign Up Free</div>
            </Link>
            <div className={'login-text noselect'}>already have an account?</div>
            <Link to="/login">
                <div className={'welcome-login welcome-item noselect'}>Log in</div>
            </Link>
            <Link to="/settings">
                <div className={'welcome-settings noselect'}>Settings</div>
            </Link>
        </div>
    );
}