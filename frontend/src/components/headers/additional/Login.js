import React from 'react';
import {Link} from "react-router-dom";
import '../../../public/css/Headers.css';

export default function Login() {
    return (
        <div className={'login-container'}>
            <Link to='/'>
                <div className={'login-item noselect home'}>Home</div>
            </Link>
            <Link to="/sign-up">
                <div className={'login-item noselect'}>Sign Up</div>
            </Link>
            <Link to="/reset-password">
                <div className={'login-item noselect'}>Reset password</div>
            </Link>
            <Link to="/settings">
                <div className={'login-item noselect'}>Settings</div>
            </Link>
        </div>
    );
}