import React from 'react';

import AvailableHeader from "./AvailableHeader";
import ProtectedHeader from "./ProtectedHeader";

export default function Header({route}) {

    switch (route) {
        case 'home':
            return <AvailableHeader route={route}/>;
        case 'reset-password':
            return <AvailableHeader route={route}/>;
        case 'login':
            return <AvailableHeader route={route}/>;
        case 'sign-up':
            return <AvailableHeader route={route}/>;
        default:
            return <ProtectedHeader />;
    }
}
